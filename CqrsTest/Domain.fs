﻿module Domain
open Cqrs

//Here are 2 bounded contexts and I'll try different approaches in implementing them
//Current goals of the research are 
//1 create 2 aggregates, 
//2 merge command and event streams into single, 
//3 create read model
//4 find generic solution

module AccountOwner =
    type AccountOwnerID = string
    type AccountOwner = {
        Id : AccountOwnerID;
        Name : string;
        Active : bool
    }

    type CreateAccountOwnerCommand =  { Id : AccountOwnerID; Name : string; Active : bool } 
    type DeactivateAccountOwnerCommand = { Id : AccountOwnerID; }
    type ActivateAccountOwnerCommand = { Id : AccountOwnerID; }

    //All commands and events should store aggregate ID
    //It requires to read it for command and event handling
    //In this approach I put aggregate ID in every command
    type IAggregateCommand = abstract member Id : AccountOwnerID with get
    type AccountOwnerCommand =
        | Create of CreateAccountOwnerCommand
        | Deactivate of DeactivateAccountOwnerCommand
        | Activate of ActivateAccountOwnerCommand
        interface IAggregateCommand with 
            member this.Id = 
                match this with
                | Create cmd -> cmd.Id
                | Deactivate cmd -> cmd.Id
                | Activate cmd -> cmd.Id


    type CreatedAccountOwnerEvent = { Id : AccountOwnerID; Name : string; Active : bool }
    type DeactivatedAccountOwnerEvent = { Id : AccountOwnerID; }
    type ActivatedAccountOwnerEvent = { Id : AccountOwnerID; }

    type IAggregateEvent = abstract member Id : AccountOwnerID with get
    type AccountOwnerEvent =
        | Created of CreatedAccountOwnerEvent
        | Deactivated of DeactivatedAccountOwnerEvent
        | Activated of ActivatedAccountOwnerEvent
        interface IAggregateEvent with
            member this.Id =
                match this with
                | Created evt -> evt.Id
                | Deactivated evt -> evt.Id
                | Activated evt -> evt.Id

    module CommandHandling = 
        let createHandler accountOwnerState (createCmd : CreateAccountOwnerCommand) =
            match accountOwnerState with
                | NotExists -> [Created { Id = createCmd.Id; Name = createCmd.Name; Active = createCmd.Active }]
                | Exists _ -> []

        let deactivateHandler (accountOwnerState : AggregateState<AccountOwner>) 
                              (deactivateCmd : DeactivateAccountOwnerCommand) = 
            match accountOwnerState with
                | Exists accountOwner when accountOwner.Active -> [Deactivated { Id = deactivateCmd.Id }]
                | _ -> []

        let activateHandler (accountOwnerState : AggregateState<AccountOwner>) 
                            (activateCmd : ActivateAccountOwnerCommand) = 
            match accountOwnerState with
                | Exists accountOwner when not accountOwner.Active -> [Activated { Id = activateCmd.Id }]
                | _ -> []
                 
        let handleAccountOwnerCommand (state : AggregateState<AccountOwner>) 
                                      (command : AccountOwnerCommand) : AccountOwnerEvent list =
            match command with
            | Create createCmd -> createHandler state createCmd            
            | Deactivate deactivateCmd -> deactivateHandler state deactivateCmd            
            | Activate activateCmd -> activateHandler state activateCmd

    module EventHandling = 
        let createdHandler state (event : CreatedAccountOwnerEvent) : AggregateState<AccountOwner> =
            Exists {Id = event.Id; Name = event.Name; Active = event.Active}

        let deactivatedHandler (state : AggregateState<AccountOwner>) (event : DeactivatedAccountOwnerEvent) =
            match state with
            | Exists accountOwner -> Exists {accountOwner with Active = false}
            | _ -> state

        let activatedHandler (state : AggregateState<AccountOwner>) (event : ActivatedAccountOwnerEvent) =
            match state with
            | Exists accountOwner -> Exists {accountOwner with Active = true}
            | _ -> state

        let applyAccountOwnerEvent (state : AggregateState<AccountOwner>) 
                                   (event : AccountOwnerEvent) : AggregateState<AccountOwner> =
            match event with
            | Created createdEvt -> createdHandler state createdEvt
            | Deactivated deactivatedEvt -> deactivatedHandler state deactivatedEvt
            | Activated activatedEvt -> activatedHandler state activatedEvt

    let getAccountOwnerId (accountOwner : AccountOwner) = accountOwner.Id
    let getAccountOwnerIdFromCommand (accOwnCmd : AccountOwnerCommand) = (accOwnCmd :> IAggregateCommand).Id
    let accountOwnerStreamConfig = {
        HandleCommand = CommandHandling.handleAccountOwnerCommand;
        ApplyEvent = EventHandling.applyAccountOwnerEvent;
        GetAggregateId = getAccountOwnerId;
        GetAggregateIdFromCommand = getAccountOwnerIdFromCommand
    }
        
module Account =
    open AccountOwner

    type Money = decimal
    type AccountID = string
    type Account = {
        Id : AccountID;
        Amount : Money;
        OwnerId : AccountOwnerID
    }

    //In this approac I moved aggregate ID into container class
    type AccountCreateCommand = { OwnerId : AccountOwnerID }
    type PutMoneyCommand = { Amount : Money }
    type TakeMoneyCommand = { Amount : Money }
    type AccountCommand = 
        | Create of AccountCreateCommand
        | Remove
        | PutMoney of PutMoneyCommand
        | TakeMoney of TakeMoneyCommand

    type AccountAggregateCommand = { AccountId : AccountID; Command : AccountCommand }

    type AccountCreatedEvent = { OwnerId : AccountOwnerID }
    type AccountPutMoneyEvent = { Amount : Money }
    type AccountTookMoneyEvent = { Amount : Money }
    type AccountEvent = 
        | Created of AccountCreatedEvent
        | Removed
        | PutMoney of AccountPutMoneyEvent
        | TookMoney of AccountTookMoneyEvent

    type AccountAggregateEvent = { AccountId : AccountID; Event : AccountEvent }

    module CommandHandling = 
        let wrapEvents (accountId : AccountID) (events : AccountEvent list) =
            events
            |> List.map (fun event -> { AccountId = accountId; Event = event })

        let handleCreate accountState 
                         (createCmd : AccountCreateCommand) 
                         (accountId : AccountID) : AccountEvent list =
            match accountState with
            | NotExists -> [Created { OwnerId = createCmd.OwnerId }]
            | Exists _ -> []

        let handleRemove accountState
                         (accountId : AccountID) : AccountEvent list =
            match accountState with
            | Exists state -> [Removed]
            | NotExists -> []

        let handlePutMoney (accountState : AggregateState<Account>)
                           (putMoneyCmd : PutMoneyCommand) 
                           (accountId : AccountID) : AccountEvent list =
            match accountState with
            | Exists state -> [PutMoney { Amount = putMoneyCmd.Amount }]
            | NotExists -> []

        let handleTakeMoney (accountState : AggregateState<Account>)
                            (takeMoneyCmd : TakeMoneyCommand) 
                            (accountId : AccountID) : AccountEvent list =
            let isEnoughMoney (account : Account) = account.Amount >= takeMoneyCmd.Amount
            match accountState with
            | Exists account when isEnoughMoney account -> [TookMoney { Amount = takeMoneyCmd.Amount }]
            | _ -> []

        let handleAccountCommand (state : AggregateState<Account>) 
                                      (aggregateCmd : AccountAggregateCommand) : AccountAggregateEvent list =
            let accountEvents = 
                match aggregateCmd.Command with
                | Create createCmd -> handleCreate state createCmd aggregateCmd.AccountId
                | Remove -> handleRemove state aggregateCmd.AccountId
                | AccountCommand.PutMoney putCmd -> handlePutMoney state putCmd aggregateCmd.AccountId
                | TakeMoney takeCmd -> handleTakeMoney state takeCmd aggregateCmd.AccountId

            wrapEvents aggregateCmd.AccountId accountEvents

    module EventHandling = 
        let applyCreatedEvent (createdEvent : AccountCreatedEvent)
                              (accountState : AggregateState<Account>)                              
                              (accountId : AccountID) =
            Exists { Account.Id = accountId; Amount = 0m; OwnerId = createdEvent.OwnerId }

        let applyRemovedEvent (accountState : AggregateState<Account>)
                              (accountId : AccountID) =
            match accountState with
            | Exists account -> NotExists
            | _ -> accountState

        let applyPutMoneyEvent (putMoneyEvent : AccountPutMoneyEvent)
                               (accountState : AggregateState<Account>)
                               (accountId : AccountID) =
            match accountState with
            | Exists account -> Exists {account with Amount = account.Amount + putMoneyEvent.Amount}
            | NotExists -> accountState

        let applyTookMoneyEvent (tookMoneyEvent : AccountTookMoneyEvent)
                                (accountState : AggregateState<Account>)
                                (accountId : AccountID) =
            match accountState with
            | Exists account -> Exists {account with Amount = account.Amount - tookMoneyEvent.Amount}
            | NotExists -> accountState

        let applyAccountEvent (state : AggregateState<Account>) 
                              (event : AccountAggregateEvent) : AggregateState<Account> =
            let handle = 
                match event.Event with
                | Created createdEvt -> applyCreatedEvent createdEvt
                | Removed -> applyRemovedEvent
                | PutMoney putEvt -> applyPutMoneyEvent putEvt
                | TookMoney tookEvt -> applyTookMoneyEvent tookEvt
            handle state event.AccountId
     
    let getAccountId (account : Account) = account.Id
    let getAccountIdFromCommand (accCmd : AccountAggregateCommand) = accCmd.AccountId
    let accountStreamConfig = {
        HandleCommand = CommandHandling.handleAccountCommand;
        ApplyEvent = EventHandling.applyAccountEvent;
        GetAggregateId = getAccountId;
        GetAggregateIdFromCommand = getAccountIdFromCommand
    }   

type DomainCommandTree =
    | AccountOwner of AccountOwner.AccountOwnerCommand
    | Account of Account.AccountAggregateCommand

type DomainEventTree =
    | AccountOwner of AccountOwner.AccountOwnerEvent
    | Account of Account.AccountAggregateEvent

type DomainInterface = {
    SendCommand: DomainCommandTree -> unit;
    EventStream: IEvent<DomainEventTree list>
}      
        
let setupDomainEventStream aggregateStorage =
    let cmdEvtStreamBuilder = new CommandEventStreamBuilder<DomainCommandTree, DomainEventTree> (aggregateStorage)

    let selectorAcc (rootCommand : DomainCommandTree) =
        match rootCommand with
        | DomainCommandTree.Account accCmd -> Some accCmd
        | _ -> None
    cmdEvtStreamBuilder.AddCommandEventProcessors Account.accountStreamConfig selectorAcc DomainEventTree.Account

    let selectorAccOwn (rootCommand : DomainCommandTree) =
        match rootCommand with
        | DomainCommandTree.AccountOwner accCmd -> Some accCmd
        | _ -> None
    cmdEvtStreamBuilder.AddCommandEventProcessors AccountOwner.accountOwnerStreamConfig selectorAccOwn DomainEventTree.AccountOwner

    let (sendCommand, outputEventStream) = cmdEvtStreamBuilder.Build ()
    
    {
        SendCommand = sendCommand
        EventStream = outputEventStream
    }