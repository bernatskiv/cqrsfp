﻿module Cqrs

type AggregateState<'a> =
    | NotExists
    | Exists of 'a

type AggregateStorage<'a, 'id, 'evt> = {
    TryGet : 'id -> AggregateState<'a>
    Append : 'id -> 'evt list -> unit
}

type CommandHandler<'a, 'cmd, 'evt> = AggregateState<'a> -> 'cmd -> 'evt list
type EventHandler<'a, 'evt> = AggregateState<'a> -> 'evt -> AggregateState<'a>

type IStorageFactory =
    abstract member Create<'a, 'id, 'evt> : ('a -> 'id) -> EventHandler<'a, 'evt> -> AggregateStorage<'a,'id,'evt>

type HandleCommand<'agg, 'cmd, 'evt> = AggregateState<'agg> -> 'cmd -> ('evt list)

type ApplyEvent<'agg, 'evt> = AggregateState<'agg> -> 'evt -> AggregateState<'agg>

type SubstreamConfig<'agg, 'cmd, 'evt, 'id> = {
    HandleCommand : HandleCommand<'agg, 'cmd, 'evt>;
    ApplyEvent : ApplyEvent<'agg, 'evt>;
    GetAggregateId : 'agg -> 'id;
    GetAggregateIdFromCommand : 'cmd -> 'id
}

type CommandEventStreamBuilder<'cmdRoot, 'evtRoot> (storageFactory : IStorageFactory) =
    let mutable processors : (('cmdRoot -> bool) * (IEvent<'cmdRoot> -> IEvent<'evtRoot list>)) list = []
    let addProcessor procPair = processors <- procPair :: processors

    let processCommand (aggStorage : AggregateStorage<'a, 'id, 'evt>) 
                        (getId : 'cmd -> 'id)
                        (commandHandler : CommandHandler<'a, 'cmd, 'evt>)
                        (command : 'cmd) =
        let aggregateId = getId command
        let aggregateState = aggStorage.TryGet aggregateId
        let events = commandHandler aggregateState command
        aggStorage.Append aggregateId events
        events

    member __.AddCommandEventProcessors<'cmd, 'evt, 'agg, 'id>          
        (subStreamConfig : SubstreamConfig<'agg, 'cmd, 'evt, 'id>)
        (selector : 'cmdRoot -> 'cmd option)       
        (evtMapper : 'evt -> 'evtRoot) = 
        let fits cmdRoot =
            match selector cmdRoot with
            | Some _ -> true
            | None -> false        

        let createEvtSubStream (rootStream : IEvent<'cmdRoot>) = 
            let aggregateStorage = storageFactory.Create subStreamConfig.GetAggregateId 
                                                          subStreamConfig.ApplyEvent
            let processCmdStream = processCommand aggregateStorage 
                                                   subStreamConfig.GetAggregateIdFromCommand 
                                                   subStreamConfig.HandleCommand 
            let eventListMapper = List.map evtMapper            
            rootStream
            |> Event.choose selector
            |> Event.map processCmdStream
            |> Event.filter (fun evts -> not evts.IsEmpty)  
            |> Event.map eventListMapper
                
        addProcessor (fits, createEvtSubStream)

    member __.Build () = 
        let input = new Event<'cmdRoot>()
        let output = new Event<'evtRoot list>()
        
        let subStreams = 
            let createSubStream (selector, createEvtSubStream : IEvent<'cmdRoot> -> IEvent<'evtRoot list>) =
                let subInput = new Event<'cmdRoot>()
                let evtSubStream = createEvtSubStream subInput.Publish
                evtSubStream.Add (fun evtList -> output.Trigger evtList)
                selector, subInput, evtSubStream
            List.map createSubStream processors    
            
        let chooseAndTrigger cmd =
            let doesStreamFit (fits, _, _) = fits cmd
            let getInput (_, inputStream, _) = inputStream
            match List.tryFind doesStreamFit subStreams with
            | Some (_, inputStream, _) -> inputStream.Trigger cmd
            | None -> ()

        input.Publish.Add chooseAndTrigger
        
        input.Trigger, output.Publish