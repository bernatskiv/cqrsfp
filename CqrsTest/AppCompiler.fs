﻿module AppCompiler
open Cqrs
open Domain

type AggregateStorageFactory () =
    interface IStorageFactory with
        member __.Create<'a,'id,'evt> getId (applyEvent) =
            let dictionary = new System.Collections.Concurrent.ConcurrentDictionary<'id, 'a>()
            let tryGet id =
                match dictionary.TryGetValue(id) with
                | true, aggregate -> Exists aggregate
                | false, _ -> NotExists
        

            let save (aggregate : 'a) id =
                let saveNew (id:'id) value = aggregate
                let saveNewDelegate = new System.Func<'id, 'a, 'a>(saveNew)
                dictionary.AddOrUpdate(id, aggregate, saveNewDelegate) 
                |> ignore
            
            let remove (id : 'id) = dictionary.TryRemove id |> ignore

            let append (id : 'id) (eventList : 'evt list) = 
                let oldAggregateState = tryGet id
                let newAggregateState = eventList |> List.fold applyEvent oldAggregateState
                match oldAggregateState, newAggregateState with
                | _, Exists aggregate -> save aggregate id
                | Exists _, NotExists -> remove id
                | NotExists, NotExists -> ()
     
            { TryGet = tryGet; Append = append }  

let createDomainEventStream () = Domain.setupDomainEventStream (new AggregateStorageFactory())