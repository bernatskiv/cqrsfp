﻿module TestExamples
open Cqrs
open Domain
open AppCompiler
open System.Collections.Generic

type AccountModel = { AccountId: Account.AccountID; Amount: Account.Money; OwnerId: AccountOwner.AccountOwnerID }
type AccountOwnerModel = { AccountOwnerId: AccountOwner.AccountOwnerID; Name: string; Active: bool }

let showMeTheShow () =
    let domainStream = AppCompiler.createDomainEventStream ()
    
    let setupAccountReadModel domainStream = 

        let accountModelStorage = new Dictionary<Account.AccountID, AccountModel>()

        let processAccountEvent (storage : Dictionary<Account.AccountID, AccountModel>)
                                (accEvt : Account.AccountAggregateEvent) =
        
            let updateItem (update : AccountModel -> AccountModel) =
                storage.[accEvt.AccountId] <- update storage.[accEvt.AccountId]

            match accEvt.Event with
            | Account.Created createEvt -> storage.Add(accEvt.AccountId, 
                                                        {
                                                            AccountId = accEvt.AccountId; 
                                                            Amount = 0m; 
                                                            OwnerId = createEvt.OwnerId
                                                        })
            | Account.Removed -> storage.Remove(accEvt.AccountId) |> ignore
            | Account.AccountEvent.PutMoney putEvt -> 
                updateItem (fun account -> {account with Amount = account.Amount + putEvt.Amount})
            | Account.AccountEvent.TookMoney tookEvt ->
                updateItem (fun account -> {account with Amount = account.Amount - tookEvt.Amount})

        let mapDomainToAccountEvents (domainEvents : DomainEventTree list) = 
            let filterAccountEvents events domainEvt =
                match domainEvt with
                | DomainEventTree.Account evt -> evt :: events
                | _ -> events
            domainEvents 
            |> List.fold filterAccountEvents []

        let processAccountEvents (accountEvents : Account.AccountAggregateEvent list) =
            accountEvents
            |> List.iter (processAccountEvent accountModelStorage)

        domainStream.EventStream
        |> Event.map mapDomainToAccountEvents
        |> Event.filter (fun evtList -> not evtList.IsEmpty)
        |> Event.add processAccountEvents

        (domainStream, accountModelStorage)

    let setupAccountOwnerReadModel domainStream = 
        let accountOwnerModelStorage = new Dictionary<AccountOwner.AccountOwnerID, AccountOwnerModel>()

        let processAccountOwnerEvent (storage : Dictionary<AccountOwner.AccountOwnerID, AccountOwnerModel>)
                                     (accOwnEvt : AccountOwner.AccountOwnerEvent) =
            let updateItem (update : AccountOwnerModel -> AccountOwnerModel) =
                let id = (accOwnEvt :> AccountOwner.IAggregateEvent).Id
                storage.[id] <- update storage.[id]
            match accOwnEvt with
            | AccountOwner.AccountOwnerEvent.Created createdEvt -> storage.Add(
                                                                    createdEvt.Id, 
                                                                    { 
                                                                        AccountOwnerId = createdEvt.Id; 
                                                                        Name = createdEvt.Name; 
                                                                        Active = createdEvt.Active
                                                                    })
            | AccountOwner.AccountOwnerEvent.Activated _ -> updateItem (fun accOwn -> {accOwn with Active = true})
            | AccountOwner.AccountOwnerEvent.Deactivated _ -> updateItem (fun accOwn -> {accOwn with Active = false})
        
        let mapDomainToAccountOwnerEvents (domainEvents : DomainEventTree list) = 
            let filterAccountEvents events domainEvt =
                match domainEvt with
                | DomainEventTree.AccountOwner evt -> evt :: events
                | _ -> events
            domainEvents 
            |> List.fold filterAccountEvents []

        let processAccountOwnerEvents = List.iter (processAccountOwnerEvent accountOwnerModelStorage)

        domainStream.EventStream
        |> Event.map mapDomainToAccountOwnerEvents
        |> Event.filter (fun evtList -> not evtList.IsEmpty)
        |> Event.add processAccountOwnerEvents 

        (domainStream, accountOwnerModelStorage)

    (setupAccountReadModel domainStream), (setupAccountOwnerReadModel domainStream)

let test () =
    let ((domainStream, accountModelStorage), (_, accountOwnerModelStorage)) = showMeTheShow()

    let sendCmd = domainStream.SendCommand
    let sendAccCmd cmd = sendCmd <| DomainCommandTree.Account cmd
    let sendAccCmdToTheAccount cmd = sendAccCmd { Account.AccountAggregateCommand.AccountId = "1";  Command = cmd }    
    let publishAccountStatus () = 
        match accountModelStorage.TryGetValue "1" with
        | true, account ->  printfn "The amount is %A" account.Amount
        | false, _ -> printfn "There is no aggregate"

    let sendAccCmdToTheAccountAndPublishStatus cmd =
        sendAccCmdToTheAccount cmd 
        publishAccountStatus ()

    sendAccCmdToTheAccountAndPublishStatus <| Account.AccountCommand.Create { Account.AccountCreateCommand.OwnerId = "Owner1" } 
    sendAccCmdToTheAccountAndPublishStatus <| Account.AccountCommand.PutMoney { Account.PutMoneyCommand.Amount = 100m }
    sendAccCmdToTheAccountAndPublishStatus <| Account.AccountCommand.TakeMoney { Account.TakeMoneyCommand.Amount = 24m }
    sendAccCmdToTheAccountAndPublishStatus <| Account.AccountCommand.PutMoney { Account.PutMoneyCommand.Amount = 1m }
    sendAccCmdToTheAccountAndPublishStatus <| Account.AccountCommand.Remove
    
    let sendAccOwnCmd cmd = sendCmd <| DomainCommandTree.AccountOwner cmd
    let accountOwnerId : AccountOwner.AccountOwnerID = "AccountOwner1";
    let sendAccOwnCmdToTheAccountOwner cmdBuilder = sendAccOwnCmd <| cmdBuilder accountOwnerId
    let publishAccountOwnerStatus () =
        match accountOwnerModelStorage.TryGetValue accountOwnerId with
        | true, { AccountOwnerId = accOwnId; Active = active } -> printfn "The user is %A and activity is %A" accOwnId active
        | false, _ -> printfn "There is no such user"
    let sendAccOwnCmdToTheAccOwnAndPublishStatus cmdBuilder =
        sendAccOwnCmdToTheAccountOwner cmdBuilder
        publishAccountOwnerStatus ()

    sendAccOwnCmdToTheAccOwnAndPublishStatus 
    <| (fun id -> AccountOwner.AccountOwnerCommand.Create { AccountOwner.CreateAccountOwnerCommand.Id = id; Name = "John"; Active = false})
    sendAccOwnCmdToTheAccOwnAndPublishStatus 
    <| (fun id -> AccountOwner.AccountOwnerCommand.Activate { AccountOwner.ActivateAccountOwnerCommand.Id = id })
    sendAccOwnCmdToTheAccOwnAndPublishStatus 
    <| (fun id -> AccountOwner.AccountOwnerCommand.Deactivate { AccountOwner.DeactivateAccountOwnerCommand.Id = id })
    
